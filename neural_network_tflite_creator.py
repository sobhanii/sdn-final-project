import tensorflow as tf
import numpy as np

(train_images, train_labels), (test_images, test_labels) = tf.keras.datasets.mnist.load_data()

train_labels = train_labels[:1000]
test_labels = test_labels[:1000]

train_images = train_images[:1000].reshape(-1, 28 * 28) / 255.0
test_images = test_images[:1000].reshape(-1, 28 * 28) / 255.0





# Convert the Keras model to a TensorFlow Lite model and write the .tflite file
tflite_model = tf.keras.models.load_model('neural_network_model.h5')
converter = tf.lite.TFLiteConverter.from_keras_model(tflite_model)
tflite_save = converter.convert()
open("generated_neural_network.tflite", "wb").write(tflite_save)

# Check the output data type and shape.
interpreter = tf.lite.Interpreter(model_path="generated_neural_network.tflite")
interpreter.allocate_tensors()

print(interpreter.get_input_details()[0]['shape'])  
print(interpreter.get_input_details()[0]['dtype']) 

print(interpreter.get_output_details()[0]['shape'])  
print(interpreter.get_output_details()[0]['dtype'])

loss, acc = tflite_model.evaluate(test_images, test_labels, verbose=2)
