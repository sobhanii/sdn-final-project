import tensorflow as tf
import numpy as np
from tensorflow import keras
from tensorflow.keras import layers

# Model / data parameters
num_classes = 10
input_shape = (28,28)

# Load the data and split it between train and test sets
(x_train, y_train), (x_test, y_test) = keras.datasets.mnist.load_data()


# reshape the input data to [batch_size, time_steps, input_dim]
x_train = x_train.reshape(x_train.shape[0], 28, 28) / 255.0
x_test = x_test.reshape(x_test.shape[0], 28, 28) / 255.0

# convert the labels to one-hot encoding
y_train = keras.utils.to_categorical(y_train)
y_test = keras.utils.to_categorical(y_test)



# convert the rnn  model to tflite format
lstm_model = tf.keras.models.load_model('lstm_model.h5')
converter_lstm = tf.lite.TFLiteConverter.from_keras_model(lstm_model)
converter_lstm.target_spec.supported_ops = [tf.lite.OpsSet.TFLITE_BUILTINS, tf.lite.OpsSet.SELECT_TF_OPS]
converter_lstm._experimental_lower_tensor_list_ops = False
tflite_model_lstm = converter_lstm.convert()

# save the rnn tflite model to a file
with open('lstm_model.tflite', 'wb') as f:
  f.write(tflite_model_lstm)


# Check the output data type and shape.
interpreter = tf.lite.Interpreter(model_path="lstm_model.tflite")
interpreter.allocate_tensors()

print(interpreter.get_input_details()[0]['shape'])  
print(interpreter.get_input_details()[0]['dtype']) 

print(interpreter.get_output_details()[0]['shape'])  
print(interpreter.get_output_details()[0]['dtype'])
tflite_model_lstm.evaluate(x_test, y_test, verbose=2)

