import numpy as np
from tensorflow import keras
from tensorflow.keras import layers
import tensorflow as tf

# Model / data parameters
num_classes = 10
input_shape = (28,28)

# Load the data and split it between train and test sets
(x_train, y_train), (x_test, y_test) = keras.datasets.mnist.load_data()


# reshape the input data to [batch_size, time_steps, input_dim]
x_train = x_train.reshape(x_train.shape[0], 28, 28) / 255.0
x_test = x_test.reshape(x_test.shape[0], 28, 28) / 255.0

# convert the labels to one-hot encoding
y_train = keras.utils.to_categorical(y_train)
y_test = keras.utils.to_categorical(y_test)

model = keras.Sequential(
    [
        keras.Input(shape=input_shape),
        layers.SimpleRNN(128, input_shape=input_shape, activation='relu'),
        layers.Dropout(0.3),
        layers.Dense(num_classes, activation="softmax"),
    ]
)

model.summary()

batch_size = 128
epochs = 15

model.compile(loss="categorical_crossentropy", optimizer="adam", metrics=["accuracy"])

model.fit(x_train, y_train, batch_size=batch_size, epochs=epochs, validation_split=0.1)

score = model.evaluate(x_test, y_test, verbose=0)
print("Test loss:", score[0])
print("Test accuracy:", score[1])

model.save('simple_rnn_model.h5')

# convert the model to tflite model
# rnn_model = tf.function(lambda x: model(x))

# BATCH_SIZE = 1
# STEPS = 28
# INPUT_SIZE = 28
# concrete_func = rnn_model.get_concrete_function(tf.TensorSpec([BATCH_SIZE, STEPS, INPUT_SIZE], rnn_model.input[0].dtype))
# # model directory.
# MODEL_DIR = "keras_rnn"
# rnn_model.save(MODEL_DIR, save_format="tf", signatures=concrete_func)

# converter = tf.lite.TFLiteConverter.from_saved_model(MODEL_DIR)
# tflite_model = converter.convert()

# save the rnn tflite model to a file
# with open('simple_rnn_model.tflite', 'wb') as f:
#     f.write(tflite_model)
