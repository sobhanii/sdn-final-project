from tflite_runtime.interpreter import Interpreter

# Load the TFLite model
interpreter = Interpreter(model_path="vgg.tflite")
interpreter.allocate_tensors()

# Get input and output tensors
input_details = interpreter.get_input_details()
output_details = interpreter.get_output_details()

# Print input and output shapes
print("Input shape:", input_details[0]['shape'])
print("Output shape:", output_details[0]['shape'])
